clc
clear all


M = 2;
K = 3;
C = 0.8;

l = 1;
m = [M C K];

G = tf(l, m);
[A, B, C, D] = tf2ss(l, m)

Z = pole(G);

for n = 1:length(Z)
    frequency(n) = sqrt(real(Z(n)).^2 + imag(Z(n)).^2);
    damping(n) = real(Z(n))/(sqrt(real(Z(n)).^2 + imag(Z(n)).^2));
end

%spr
damp(G); 
frequency
damping


