clc
clear all
close all
l = 1;

for m = 6:4:14  %l. biegun�w
    
    for n = 0:(m-1)
        Re_z(n+1) = (round(10000*(cosd((360/m)*n))))/10000;
        Im_z(n+1) = (round(10000*(sind((360/m)*n))))/10000;
        
        figure(2*l - 1)
        subplot(1,2,1)
        plot(Re_z(n+1),Im_z(n+1),'bx');
        line([0 0], ylim, 'Color','black','LineStyle',':');
        line(xlim, [0 0], 'Color','black','LineStyle',':');
        title("Uk�ad o " + m + " biegunach (f.plot)");
        xlabel("Real axis [ s^{-1} ]"), ylabel("Imaginary axis [ s^{-1] }")
        hold on
        
        
        zro(n+1) = (Re_z(n+1) + i*Im_z(n+1));
    end
    
    zro1 = zro(find(real(zro) < 0));
    sys = zpk([], zro1, 1);
    
    subplot(1,2,2)
    pzmap(sys), grid on
    title("Uk�ad o " + m/2 + " biegunach (f. pzmap)");
    
    figure(2*l)
    time=linspace(0,40,400);
    step(sys, time), grid on, grid minor, 
    title("Odpowiedz uk�adu o " + m/2 + " biegunach na skok jednostkowy");
    skok = step(sys, time);
    
    S(l) = stepinfo(sys)
    
    l = l+1;
end
   