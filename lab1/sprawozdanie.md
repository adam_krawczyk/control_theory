    
## Zadanie 1
 
### Treść zadania :

```
Rozważ układ masa – sprężyna - tłumik M = 2, K = 3, C = 0.8. Zapisz transmitancję
operatorową obiektu G(s) = X(s)/F(s) gdzie X(s) to przemieszczenie masy M a F(s) to siła
wymuszająca przyłożona do masy M. Oblicz częstość drgań własnych i współczynnik
tłumienia układu. Przekształć model transmitancyjny w model przestrzeni stanu.
```

### Rozwiązanie :

```
M = 2;
K = 3;
C = 0.8;
 
l = 1;
m = [M C K];
 
G = tf(l, m);
[A, B, C, D] = tf2ss(l, m)
 
Z = pole(G);
 
for n = 1:length(Z)
    frequency(n) = sqrt(real(Z(n)).^2 + imag(Z(n)).^2);
    damping(n) = real(Z(n))/(sqrt(real(Z(n)).^2 + imag(Z(n)).^2));
end
 
%spr
damp(G); 
frequency
damping
```


### Wyniki :

| Pole        | Damping           | Frequency  |Time Constant  |
| ------------- |:-------------:| :----:| :-------------|
| -2.00e-01 + 1.21e+00i      | 1.63e-01 | 1.22e+00 |      5.00e+00 |
| -2.00e-01 - 1.21e+00i    | 1.63e-01      |   1.22e+00 |    5.00e+00           |



|Frequency| Damping|
| :------------- |:-------------:|
|    1.2247    |-0.1633|   
|    1.2247     |-0.1633|


A = [-0.4000   -1.5000;
     1.0000         0]


B = [1;   0]


C = [0;   0.5000]


D = [0]


### Wnioski :

-

## Zadanie 2
 
### Treść zadania :

```
a. Określ 10 biegunów jak następuje: biegun nr 1 znajduje się w punkcie +1. Bieguny są
równomiernie rozmieszczone na okręgu jednostkowym na płaszczyźnie liczb
zespolonych. Następnie skonstruuj układ nie posiadający zer z pięcioma biegunami (z
powyższych dziesięciu), leżącymi po lewej stronie osi liczb urojonych.

b. Wygeneruj i wykreśl odpowiedź takiego układu na skok jednostkowy. Wylicz czas do
pierwszego maksimum, przeregulowanie, czas wzrostu, czas ustalania i wartość końcową.

c. Powtórz podpunkty a i b dla 6 i 14 biegunów. Jak zmieniają się własności układu –
wyjaśnij zachodzące zjawiska.
```

### Rozwiązanie :

```
l = 1;

for m = 6:4:14  %l. biegun�w
    
    for n = 0:(m-1)
        Re_z(n+1) = (round(10000*(cosd((360/m)*n))))/10000;
        Im_z(n+1) = (round(10000*(sind((360/m)*n))))/10000;
        
        figure(2*l - 1)
        subplot(1,2,1)
        plot(Re_z(n+1),Im_z(n+1),'bx');
        line([0 0], ylim, 'Color','black','LineStyle',':');
        line(xlim, [0 0], 'Color','black','LineStyle',':');
        title("Uk�ad o " + m + " biegunach (f.plot)");
        xlabel("Real axis [ s^{-1} ]"), ylabel("Imaginary axis [ s^{-1] }")
        hold on
        
        
        zro(n+1) = (Re_z(n+1) + i*Im_z(n+1));
    end
    
    zro1 = zro(find(real(zro) < 0));
    sys = zpk([], zro1, 1);
    
    subplot(1,2,2)
    pzmap(sys), grid on
    title("Uk�ad o " + m/2 + " biegunach (f. pzmap)");
    
    figure(2*l)
    time=linspace(0,40,400);
    step(sys, time), grid on, grid minor, 
    title("Odpowiedz uk�adu o " + m/2 + " biegunach na skok jednostkowy");
    skok = step(sys, time);
    
    S(l) = stepinfo(sys)
    
    l = l+1;
end

```


### Wyniki :



### Wnioski :

Przy zwiększającej się liczbie biegunów bieguny dominujące mają zwiększający się współczynnik tłumienia co powoduje, że jeszcze bardziej wpływają na odpowiedź układu

## Zadanie 3
 
### Treść zadania :

```
Wykreśl charakterystykę Nyquista dla układu G(s) = 1 / (s+20) (s + 0.001) (s+1). Powiększ
obszar w pobliżu początku układu współrzędnych. Wyświetl tylko obszar -0.003 ≤ Re G(jω)
≤ 0 i -0.0005≤ Im G(jω) ≤ 0.0005. Skąd wiadomo, że ten region będzie interesujący?
```

### Rozwiązanie :

```
L = [1];
M = [1,21.001,20.021,0.02];
sys = tf(L,M);

nyquist(sys);
```


### Wyniki :



### Wnioski :

Widadomo, że ten obszar będzie interesujący ponieważ mimo 3 biegunów wykres wygląda tak jakby nie przecinał 3 ćwiartek. Po przybliżeniu widać, że jednak przecina.