clear all
clc
close all

s = tf('s');

GC_G = (s + 1)*(s + 3)/(s*(s -1)*(s + 4)*(s + 8));
% nie wiem czy w poleceniu jest błąd, ale w liczniku jest ("l") a nie 1 -
% zakładam, że jest 1.
%założyłem, że K = 1 

figure(1)
rlocus(GC_G)
%sisotool(GC_G)


% Z pomocą narzędzia sisotool dobrałem odpowiedni parametr K = 37.982
% Użyłem tego narzędzia ze względu na możliwość dynamicznego obserwowania
% zmiany parametrów dla każdego bieguna jednocześnie.
K = 37.982;
sys = feedback(GC_G*(K),1);
step(sys)
s = stepinfo(sys)
%Wartość przeregulowania wynosi 49,8762%

%	Wszystkie parametry:
%         RiseTime: 0.2535
%     SettlingTime: 2.3644
%      SettlingMin: 0.9302
%      SettlingMax: 1.4988
%        Overshoot: 49.8762
%       Undershoot: 0
%             Peak: 1.4988
%         PeakTime: 0.8001
