## Ćwiczenia laboratoryjne:

Proszę wykonać zadania z instrukcji nr 3, która znajduje się na serwerze przedmiotu. Z wykonanych zadań należy sporządzić sprawozdanie zgodnie z zasadami, które określiłem na początku zajęć laboratoryjnych i odesłać na mój e-mail do 09.04.2020 do godziny 23:59. W dalszym ciągu obowiązuje Państwa praca w zespołach. Poniżej moje uwagi do zadań:

1. Należy wykreślić dwa wykresy mgp dla obu transmitancji z podpunktów a i b stosując polecenie rlocus a następnie przeanalizować je i odpowiedzieć na pytanie z podpunktu c.


2. Należy wykreślić rodzinę wykresów mgp dla transmitancji ze zmieniającym się parametrem a. Proponuję zastosować pętlę for lub while i nie umieszczać wszystkich mgp w jednym układzie współrzędnych – raczej osobne wykresy. Krok zmiany parametru a należy wybrać samodzielnie na rozsądnym poziomie – tak aby wykresów nie był zbyt wiele, a wartość parametru znaleziona.

3. Podpunkty a i b są dosyć oczywiste. Ponownie zalecam umieszczanie kolejnych mgp na osobnych wykresach. Komentarze i uwagi będące efektem analiz z punktu c należy oprzeć na fakcie, iż projektanci układów sterowania dążą do zniesienia wpływu niekorzystnych biegunów przez umieszczenie w transmitancji regulatora zer o takiej samej bądź zbliżonej wartości. Matematyczny efekt skracania się takich samych członów w liczniku i mianowniku ma przełożenie na fizyczne zachowanie się układów.

4. a) W pierwszym kroku warto przekształcić model układu do formy transmitancyjnej i odczytać położenia zer i biegunów. Następnie określamy, które biegun są dominujące i odczytujemy ich częstość drgań własnych i współczynnik tłumienia (np. poleceniem damp). W kolejnym kroku tworzymy transmitancję filtru notch poleceniem zpk. Zera filtra będą stanowiły dwa znalezione uprzednio bieguny dominujące układu, a bieguny filtra musimy utworzyć ręcznie jako dwie liczby zespolone sprzężone o identycznej częstości drgań własnych jak bieguny dominujące układu (zera filtra) i 5-krotnie większym współczynniku tłumienia (5-krotnie większym cosinusie kąta theta). Proponuję zastosować formę trygonometryczną lub Eulera do tworzenia tych dwóch liczb zespolonych. Wzmocnienie filtra (trzeci parametr polecenia zpk) powinno wynosić 1.

4. b) i c). Należy tę wartość niedokładności („nieco”) pozycjonowania zer filtra i biegunów dominujących układu tak dobrać, aby odpowiednie gałęzie mgp zaczynały się w rzeczonych biegunach dominujących obiektu i dążyły do zer filtra.

4 d) i e). Kluczowy jest kierunek wyginania się gałęzi mgp z podpunktu b i c.

5. Nie wykonujemy.

6. W tym zadaniu pomocna będzie część wykładu, którą wystawiłem dla Państwa na dysku Google. Zadanie jest w zasadzie dobrze opisane, można dodać wskazówki, że linie stałego tłumienia na mgp w Matlabie wykreślamy poleceniem sgrid, a wartość wzmocnienia dla danego położenia biegunów układu zamkniętego  odczytujemy przez kliknięcie myszką w gałąź mgp w wybranym punkcie. Proszę pamiętać o zamknięciu pętli sprzężenia zwrotnego przy sprawdzaniu poprawności otrzymanego rozwiązania.
