clc 
clear
close all

num = [1];
 
a = 0.01;

for i=0:4
den = [1 (a+1) (4+a) 4 0];
sys = tf(num,den);
figure();
rlocus(sys)
title(['MGP for a = ',num2str(a)])
a = a + 1;
end

%dla a = 0.01 układ jest niestabilny bo posiada bieguny na dodatniej części
%osi liczb rzeczywistych 

%przy zmianie wartości a na coraz większe dochodzi do przesunięcia biegunów
%dominujących w stronę ujemnych wartości osi liczb rzeczywistych

%Zmiana biegunów zmierzających w stronę niestabilnosci następuje dla ~3.98