clc 
clear
close all

num = [3 -4];
den = [1 -2 2];
sys = tf(num,den);

figure(1);
rlocus(sys)

num = [-3 4];
den = [1 -2 2];
sys2 = tf(num,den);

figure(2);
rlocus(sys2)

isstable(sys)
isstable(sys2)

Gm = margin(sys)
Gm2 = margin(sys2)


% Wnioski:
% Układ 1 otwarty jest stabilny dla współczynnika wzmocnienia 0.5. Układ 2
% jest niestabilny. Po zamknięciu pętli oba układy są niestabilne.