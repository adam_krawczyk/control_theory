clc 
clear
close all

% a)
den = [1 3 8.5 12 10.8125 5.3125];
a = 1.25;

for i=0:3
num = [1 1 a];
sys = tf(num,den);
figure();
h = rlocusplot(sys);
p = getoptions(h); % Get options for plot.
p.Title.String = sprintf('a) mgp dla wsp wzmocnienia %f', a); 
setoptions(h,p); % Apply options to plot.
a = a + 1;
end

% b)
num = [1 6 41.5 126 417.0625 598.125 1040.625]; 
den = [1 6 34 96 173 170 0];

figure();
sys = tf(num,den);
h = rlocusplot(sys);
p = getoptions(h); 
p.Title.String = sprintf('b) mgp'); 
setoptions(h,p); 


% oba układy są stabilne. 
% układ a) prawdopodobnie najlepiej będzie się zachowywał dla wartośći
% a~1.25 ponieważ ma wtedy najbardziej skupione bieguny i zera ale ma wolną
% reakcje na wymuszenie oraz małą częstotliwość oscylacji.

% układ b) posiada w dużej odległości od siebie wszystkie zera i bieguny
% co prawdopodobnie będzie utrudniało optymalne dostrojenie regulatora.
% 





